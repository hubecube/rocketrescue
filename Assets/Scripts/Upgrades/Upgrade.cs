﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Upgrade
{
	
	[HideInInspector]
	public int upgradeLevel = 0;

	public float[] tiers;
	public int[] costs;

	public bool CheckCost(int money)
	{
		if (money > costs[upgradeLevel])
			return true;
		return false;
	}

	public float GetUpgrade()
	{
		return tiers[upgradeLevel];
	}

	public int BuyUpgrade(int money)
	{
		
		int currentMoney = money;
		if (CheckCost(money))
		{
			currentMoney -= costs[upgradeLevel];
			if (upgradeLevel < tiers.Length - 1)
				upgradeLevel++;
		}
		else
		{
			Debug.Log("You dont have enough points");
		}
		return currentMoney;
	}




}
