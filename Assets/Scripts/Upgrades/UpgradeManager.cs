﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UpgradeManager : MonoBehaviour
{
	public Upgrade speedUpgrade;
	public Upgrade fuelUpgrade;
	public Upgrade rotationUpgrade;
	public Upgrade landingGearUpgrade;

	public void BuySpeedUpgrade()
	{
		int lastPoints = ScoreManager.totalPoints;
		ScoreManager.totalPoints = speedUpgrade.BuyUpgrade(ScoreManager.totalPoints);
		if (lastPoints > ScoreManager.totalPoints)
		{
			ObjRef.playerMove.thrustFactor *= speedUpgrade.GetUpgrade();
			ObjRef.playerMove.maxSpeed *= speedUpgrade.GetUpgrade();

		}
	
	}

	public void BuyFuelUpgrade()
	{
		int lastPoints = ScoreManager.totalPoints;
		ScoreManager.totalPoints = fuelUpgrade.BuyUpgrade(ScoreManager.totalPoints);
		if (lastPoints > ScoreManager.totalPoints)
		{
			ObjRef.playerMove.fuel *= fuelUpgrade.GetUpgrade();

		}
	}

	public void BuyRotationUpgrade()
	{
		int lastPoints = ScoreManager.totalPoints;
		ScoreManager.totalPoints = rotationUpgrade.BuyUpgrade(ScoreManager.totalPoints);
		if (lastPoints > ScoreManager.totalPoints)
		{
			ObjRef.playerMove.rotationFactor *= rotationUpgrade.GetUpgrade();
		}
	}

	public void BuyLandingGearUpgrade()
	{
		int lastPoints = ScoreManager.totalPoints;
		ScoreManager.totalPoints = landingGearUpgrade.BuyUpgrade(ScoreManager.totalPoints);
		if (lastPoints > ScoreManager.totalPoints)
		{
			ObjRef.playerTransform.GetComponent<PlayerCollision>().speedThreshhold *= landingGearUpgrade.GetUpgrade();
		}

	}

}
