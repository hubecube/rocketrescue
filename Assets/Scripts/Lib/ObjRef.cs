﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjRef : Singleton<ObjRef>
{

	public static Transform playerTransform;
	public static PlayerMovement playerMove;
	public static Rigidbody playerRb;
	public static CameraFollowOld cam;

	public static Canvas hudUI;
	public static MenuController menuUI;
	public static CollisionController collisionUI;
	public static SummaryController summaryUI;
	public static SolarFlareController endUI;

	public static Transform landingZone;



	void Awake()
	{
		playerTransform = GameObject.Find("Player").transform;
		playerMove = playerTransform.GetComponent<PlayerMovement>();
		playerRb = playerTransform.GetComponent<Rigidbody>();

		menuUI = FindObjectOfType<MenuController>();
		cam = FindObjectOfType<CameraFollowOld>();
		collisionUI = FindObjectOfType<CollisionController>();
		summaryUI = FindObjectOfType<SummaryController>();
		endUI = FindObjectOfType<SolarFlareController>();
	}


}
