﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScoreView : MonoBehaviour
{
	Text scoreText;

	void Awake()
	{
		scoreText = GetComponent<Text>();
		ScoreManager.OnFinalScoreUpdate += HandleScoreUpdate;	
	}

	void OnDestroy()
	{
		ScoreManager.OnFinalScoreUpdate -= HandleScoreUpdate;
	}

	void HandleScoreUpdate(float scientists, float time, int finalScore, int bestScore)
	{
		scoreText.text = "SCIENTISTS RESCUED: " + scientists + "\nFINAL SCORE: " + finalScore + "\nTIME: " + time + "\n\n" + "BEST SCORE: " + bestScore;


	}

}
