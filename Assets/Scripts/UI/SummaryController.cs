﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SummaryController : MonoBehaviour
{

	public PopUp playBtn;

	void OnEnable()
	{
		GameManager.OnStateChange += HandleOnGameStateChange;
	}

	void OnDisable()
	{
		GameManager.OnStateChange -= HandleOnGameStateChange;
	}

	void HandleOnGameStateChange(GameState state)
	{

		switch (state)
		{
		case GameState.MainMenu:
			playBtn.Show();
			break;
		case GameState.StartGame:
			playBtn.Hide();
			break;
		}


	}
}
