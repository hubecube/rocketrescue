﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerGravity : MonoBehaviour
{

	public List<Asteroid> asteroids;
	public float force = 10;

	public Asteroid currentAsteroid;
	float closestDistance = 1000;

	Rigidbody rb;

	void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	IEnumerator Start()
	{
		yield return new WaitForEndOfFrame();
		StartCoroutine(CheckAsteroids(1));
	}

	void ApplyGravity(Asteroid obj, float force)
	{
		rb.AddForce((transform.position - obj.transform.position) * (1 / closestDistance) * (Physics.gravity.y * obj.radius * force * Time.deltaTime));
	}

	void FixedUpdate()
	{
		if (currentAsteroid != null)
		{
			ApplyGravity(currentAsteroid, force);
		}
	}


	IEnumerator CheckAsteroids(float delay)
	{
		while (true)
		{
			currentAsteroid = FindClosestAsteroid(asteroids);
			yield return new WaitForSeconds(delay);
			yield return null;
		}
		yield return null;
	}


	Asteroid FindClosestAsteroid(List<Asteroid> asteroidsList)
	{
		if (asteroidsList.Count > 0)
		{
			closestDistance = Vector3.Distance(transform.position, asteroidsList[0].transform.position);
			int index = 0;
			for (int i = 0; i < asteroidsList.Count; i++)
			{
				float dist = Vector3.Distance(transform.position, asteroidsList[i].transform.position);
				if (dist < closestDistance)
				{
					closestDistance = dist;
					index = i;

				}
			}
			closestDistance = Mathf.Clamp(closestDistance, 0.1f, 1000);
//			print(asteroidsList[index]);
			return asteroidsList[index];
		}
		return null;
	}


}
