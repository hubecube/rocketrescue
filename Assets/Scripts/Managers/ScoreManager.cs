﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScoreManager : Singleton<ScoreManager>
{

	public delegate void OnScoreEvent(float scientistsRescued, float time, int finalScore, int bestScore);

	public static event OnScoreEvent OnFinalScoreUpdate;

	public static int totalPoints;

	public const string HIGH_SCORE = "bestScore";

	public float scientistsRescued;
	public float time;
	public int finalScore;

	public int bestScore;

	void OnEnable()
	{
		GameManager.OnStateChange += HandleOnStateChange;
	}


	void OnDisable()
	{
		GameManager.OnStateChange -= HandleOnStateChange;
	}

	void HandleOnStateChange(GameState nextState)
	{
		if (nextState == GameState.Game)
		{
			time = 0;
			StartCoroutine(LevelTimer());
		}
		else if (nextState == GameState.Summary)
		{
			StopAllCoroutines();
			CalculateScore();
			StorePrefs();
			totalPoints += finalScore;
			if (OnFinalScoreUpdate != null)
				OnFinalScoreUpdate(scientistsRescued, time, finalScore, bestScore);
			
		}
	}

	void Awake()
	{
		Init();
	}

	void Init()
	{
		GetPrefs();
		time = 0;
		scientistsRescued = 0;
		finalScore = 0;
	}

	public void AddScientist()
	{
		scientistsRescued += 1;
	}

	IEnumerator LevelTimer()
	{
		while (true)
		{
			time += 1;
			yield return new WaitForSeconds(1);
			yield return null;
		}
		yield return null;
	}

	void CalculateScore()
	{
		float f;
		if (scientistsRescued <= 0)
			f = time;
		else
			f = (scientistsRescued * time);
		finalScore = (int)f;
	}

	void GetPrefs()
	{
		if (!PlayerPrefs.HasKey(HIGH_SCORE))
		{
			PlayerPrefs.SetInt(HIGH_SCORE, 0);
			bestScore = 0;
		}
		else
		{
			bestScore = PlayerPrefs.GetInt(HIGH_SCORE);
			//			print(bestScore);	
		}


	}

	public void StorePrefs()
	{
		if (PlayerPrefs.GetInt(HIGH_SCORE) < finalScore)
		{
			PlayerPrefs.SetInt(HIGH_SCORE, finalScore);
		}
	}


}
