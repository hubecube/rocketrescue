﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Flags]
public enum GameState
{
	NullState = 1 << 0,
	Intro = 1 << 1,
	MainMenu = 1 << 2,
	Instructions = 1 << 3,
	//just incase we add this in
	Shop = 1 << 4,
	StartGame = 1 << 5,
	Game = 1 << 6,
	Summary = 1 << 7,
	End = 1 << 8,
	Collision = 1 << 9,
}


public class GameManager : Singleton<GameManager>
{
	public const string FIRSTLEVEL = "__init";
	public const string SECONDLEVEL = "__init 1";



	public GameState gameState { get; private set; }

	public GameState nextState = GameState.Intro;
	public GameState currentState = GameState.NullState;
	public GameState lastState = GameState.NullState;

	public Graphic background;

	public CollisionController collisionUI;
	public SolarFlareController endUI;
	public SummaryController summaryUI;

	public float durationBeforeFlare = 10;
	private float origDuration;
	public float rescueIncrement = 3;

	int numCrashes = 0;

	#region event declaration

	public delegate void OnStateChangeHandler(GameState nextState);

	public static event OnStateChangeHandler OnStateChange;

	public delegate void OnStateUpdate();

	public static event OnStateUpdate OnStateUpdateHandler, OnSwipeLeft, OnSwipeRight;

	public delegate void OnNewStartPosition(Vector3 pos, Quaternion rot);

	public static event OnNewStartPosition OnSetPosition;

	#endregion




	#region Monobehaviours

	void Awake()
	{
		OnStateChange += HandleOnStateChange;
		origDuration = durationBeforeFlare;
		Application.targetFrameRate = 60;
		#if UNITY_EDITOR
		Application.runInBackground = true;
		#endif
	}

	void Start()
	{
		if (instance == this)
			SetGameState(GameState.Intro);
	}

	void OnDestroy()
	{
		OnStateChange -= HandleOnStateChange;
	}

	void Update()
	{
		//for modularly adding/removing functionality from a state's update loop
		if (OnStateUpdateHandler != null)
			OnStateUpdateHandler();
		
	}

	void OnLevelWasLoaded(int level)
	{
		
	}

	#endregion


	#region EventHandlers


	public void SetGameState(GameState gameState)
	{
		if (gameState != currentState)
		{
			lastState = currentState;

			if (OnStateChange != null)
			{
				OnStateChange(gameState);
			}
			currentState = gameState;
		}
		else
		{
			Debug.Log("ALREADY IN THIS STATE");

		}

	}

	public static void SwipeRight()
	{
		if (OnSwipeRight != null)
		{
			OnSwipeRight();
		}
	}

	public static void SwipeLeft()
	{
		if (OnSwipeLeft != null)
		{
			OnSwipeLeft();
		}
	}

	#endregion

	#region Public Methods

	public void PrepareGame()
	{
		SetGameState(GameState.StartGame);
	}

	public void ResetGame()
	{
		SetGameState(GameState.MainMenu);
//		LoadLevel(FIRSTLEVEL);
	}

	public void NextLevel()
	{
//		LoadLevel(SECONDLEVEL);
	}

	#endregion

	void HandleOnStateChange(GameState state)
	{
		OnStateUpdateHandler = null;
		//				inTransition = true;
		OnStateUpdateHandler += () => {
			if (Input.GetKeyDown(KeyCode.Space))
				SetGameState(nextState);
		};
		switch (state)
		{
		case GameState.Intro:
			D.log("intro state");
		
			nextState = GameState.MainMenu;
			ObjRef.cam.enabled = false;
			SetGameState(nextState);
			break;
		case GameState.MainMenu:
			ObjRef.cam.enabled = false;
			ObjRef.playerMove.Disable();
//			ObjRef.hudUI.gameObject.SetActive(false);
			ObjRef.menuUI.gameObject.SetActive(true);
			ObjRef.menuUI.playBtn.Show();


			nextState = GameState.StartGame;
			break;
		case GameState.StartGame:
			ObjRef.playerMove.Enable();
			ObjRef.playerMove.StartGame();
			nextState = GameState.Game;
			break;
		case GameState.Game:
			OnStateUpdateHandler += () => {
				if (durationBeforeFlare > 0)
				{
					durationBeforeFlare -= Time.deltaTime;
				}
				else if (durationBeforeFlare <= 0)
				{
					SetGameState(GameState.End);
				}
			};
			ObjRef.menuUI.gameObject.SetActive(false);
			ObjRef.cam.enabled = true;
			nextState = GameState.Summary;
			break;
		case GameState.Summary:
			durationBeforeFlare = origDuration;
			summaryUI.playBtn.Show();
			ObjRef.playerMove.Reset();
			nextState = GameState.End;
			break;
		case GameState.End:
//			ObjRef.interstitialUI.gameObject.SetActive(true);

			ObjRef.playerMove.Disable();
//			ObjRef.hudUI.gameObject.SetActive(false);
			nextState = GameState.StartGame;
			endUI.gameObject.SetActive(true);
			endUI.playBtn.Show(() => {
					StartCoroutine(Auto.Wait(1, () => {
								endUI.playBtn.Hide(() => {
										SetGameState(GameState.Summary);
									});
							}));
				});

			break;
		case GameState.Collision:
			ObjRef.playerMove.Disable();
			collisionUI.gameObject.SetActive(true);
			collisionUI.playBtn.Show(() => {
					StartCoroutine(Auto.Wait(1, () => {
								collisionUI.playBtn.Hide(() => {
										SetGameState(GameState.Summary);
									});
							}));
				});


			numCrashes += 1;
			nextState = GameState.StartGame;

			break;
		}
		Debug.Log("switching state to " + state);

	}

}