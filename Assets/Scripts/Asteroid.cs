﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Asteroid : MonoBehaviour
{
	[HideInInspector]
	public Collider col;

	public float radius;

	[HideInInspector]
	public Rigidbody rb;
	//	public float gravityFactor;

	void Awake()
	{
		if (GetComponent<Collider>())
		{
			col = GetComponent<Collider>();
		}
		else if (GetComponentInChildren<Collider>())
		{
			col = GetComponentInChildren<Collider>();
		}
		radius = col.bounds.extents.x;
		if (GetComponent<Rigidbody>())
			rb = GetComponent<Rigidbody>();
	}

	void Start()
	{
		ObjRef.playerTransform.GetComponent<PlayerGravity>().asteroids.Add(this);


	}

	void OnDisable()
	{
		if (ObjRef.playerTransform != null)
			ObjRef.playerTransform.GetComponent<PlayerGravity>().asteroids.Remove(this);
	}


}
