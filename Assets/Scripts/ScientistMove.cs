﻿using UnityEngine;
using System.Collections;

public class ScientistMove : MonoBehaviour {

    //This will assign layer 8
    private int targetLayer_ = (1 << 9);
    private int playerTargetLayer_ = (1 << 9 | 1 << 12);
    private GameObject playerGO_, scientistGO_;
    private int runningLeftRight_ = 1;
    private Animator anim_;
    private bool iSeeTheShip_ = false;          //true when doesn't see ship
    private RaycastHit hit_ = new RaycastHit();

    // Use this for initialization
    void Start () {

        //get variables
        anim_ = this.GetComponentInChildren<Animator>();
        scientistGO_ = anim_.gameObject;
        playerGO_ = GameObject.Find("Player");

        //Physics.Raycast returns true if it hits the target layer
        if (Physics.Raycast((this.transform.position + (this.transform.up * 5) + this.transform.right), -this.transform.up, out hit_, 1000.0f, targetLayer_))
        {
            this.transform.position = hit_.point + this.transform.up;
        }
    }
	
	// Update is called once per frame
	void Update () {

        //rotate the player away from center
        this.transform.up = (this.transform.position - this.transform.parent.position);

        //can the scientists see the ship ?
        if (Physics.Raycast(this.transform.position, playerGO_.transform.position - this.transform.position, out hit_, 1000.0f, playerTargetLayer_))
        {
            //if we hit the player
            if (hit_.transform.gameObject.layer == 12)
            {
                iSeeTheShip_ = true;
                scientistGO_.transform.localEulerAngles = new Vector3(0, 180, 0);
            }
            else
            {
                if (iSeeTheShip_)
                {
                    iSeeTheShip_ = false;

                    if (Random.Range(-1, 1) < 0)
                        runningLeftRight_ = -1;
                    else
                        runningLeftRight_ = 1;

                    scientistGO_.transform.localEulerAngles = new Vector3(0, 90 * runningLeftRight_, 0);
                }
            }
        }
        else {
            if (iSeeTheShip_)
            {
                iSeeTheShip_ = false;

                if (Random.Range(-1, 1) < 0)
                    runningLeftRight_ = -1;
                else
                    runningLeftRight_ = 1;

                scientistGO_.transform.localEulerAngles = new Vector3(0, 90 * runningLeftRight_, 0);
            }
        }


        //if the player doesnt see the ship
        if (!iSeeTheShip_) {
            anim_.SetFloat("FreakOut", 0);
            //Physics.Raycast returns true if it hits the target layer
            if (Physics.Raycast((this.transform.position + (this.transform.up * 5) + (runningLeftRight_ * this.transform.right)), -this.transform.up, out hit_, 1000.0f, targetLayer_))
            {
                this.transform.position = hit_.point + this.transform.up;
            }
        }
        else
        {
            anim_.SetFloat("FreakOut", Random.Range(1, 3.5f));
        }

    }

}
