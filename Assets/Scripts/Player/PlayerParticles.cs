﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerParticles : MonoBehaviour
{

	public const float velocityFactor = 0.3f;

	public ParticleSystem leftEngine;
	public ParticleSystem rightEngine;

	Rigidbody rb;


	float origStartSpeed;

	void Start()
	{
		rb = ObjRef.playerRb;
		origStartSpeed = leftEngine.startSpeed;
	}


	public void FireEngines(float leftInput, float rightInput)
	{
		if (leftInput != 0)
		{
			FireLeftEngine();
		}
		else if (leftInput == 0)
		{
			StopLeftEngine();
		}

		if (rightInput != 0)
		{
			FireRightEngine();
		}
		else if (rightInput == 0)
		{
			StopRightEngine();

		}

	}

	void FixedUpdate()
	{
		
		float v = (((transform.up * rb.velocity.magnitude) - rb.velocity).magnitude) * velocityFactor;
//		if ()
		leftEngine.startSpeed = origStartSpeed + v;
		rightEngine.startSpeed = origStartSpeed + v;
	}



	void FireLeftEngine()
	{
		

		leftEngine.enableEmission = true;

//			leftEngine.Play();
		
	}

	void FireRightEngine()
	{
		

		rightEngine.enableEmission = true;
//			rightEngine.Play();
		
	}


	void StopLeftEngine()
	{
		

		leftEngine.enableEmission = false;
//			leftEngine.Pause();

	}

	void StopRightEngine()
	{
		
		rightEngine.enableEmission = false;
//			rightEngine.Pause();

	}
}
