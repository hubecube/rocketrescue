﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerCollision : MonoBehaviour
{

	public float speedThreshhold = 5;

	void OnCollisionEnter(Collision col)
	{
		print(ObjRef.playerRb.velocity.sqrMagnitude);
		if (ObjRef.playerRb.velocity.sqrMagnitude > speedThreshhold)
		{
			print("player collided");
			GameManager.instance.SetGameState(GameState.Collision);
		}
		else
		{
			
		}
	}

}
