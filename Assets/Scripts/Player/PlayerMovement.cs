﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum PlayerState
{
	NullState,
	Intro,
	Flying,
	Landing,
	Landed,
	Crashed,
	Takeoff,
	Dead
}


public class PlayerMovement : MonoEx
{
	[HideInInspector]
	public bool canInput;

	public LayerMask mask;

	[HideInInspector]
	public PlayerState currentState;
	PlayerState lastState;

	public float startDuration = 3f;

	public float inputIncrement = 0.1f;

	public float thrustFactor;
	public float initialThrust = 200;
	public float rotationFactor;

	public float maxSpeed = 20;

	private float currentThrust;
	private float currentRotation;

	public float fuel = 30f;
	float currentFuel;
	// in seconds;

	float rightInput;
	float leftInput;

	float origThrust;

	PlayerParticles particles;
	//	private Rigidbody rb;



	protected override void Awake()
	{
		base.Awake();

		SwitchState(PlayerState.Intro);

	}

	protected override void Init()
	{
		base.Init();
		particles = GetComponent<PlayerParticles>();
		origThrust = thrustFactor;

	}

	void GetInput()
	{
		Touch[] myTouches = Input.touches;
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (myTouches[i].position.x < Screen.width / 2)
				leftInput = 1;
			else if (myTouches[i].position.x >= Screen.width / 2)
			{
				rightInput = 1;
			}
			//Do something with the touches
		}
	}




	void Update()
	{
		
		if (canInput)
		{
		
			leftInput = 0;
			rightInput = 0;
			
//			if (Input.GetMouseButton(0) && Input.mousePosition.x < Screen.width / 2)
			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
			{
				leftInput = 1;

			}

//			if (Input.GetMouseButton(0) && Input.mousePosition.x >= Screen.width / 2)
			if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
			{
				rightInput = 1;

			}

			GetInput();
			fuel -= (Time.deltaTime * (rightInput + leftInput));
			if (fuel <= 0)
			{
				canInput = false;	
				GameManager.instance.SetGameState(GameState.End);
			}
		}
		particles.FireEngines(leftInput, rightInput);
	}


	// make this generic later. also make it take in input and return output
	// so it can be used for player and also AI or if multiplayer later,


	void FixedUpdate()
	{
		float f = CheckDistance();
		//Force
		if (leftInput != 0 || rightInput != 0)
		{
			float factor = 1;
			if (leftInput != rightInput)
			{
				factor = 0.25f;
			}
			Vector3 force = transform.up * factor * thrustFactor * (leftInput + rightInput);
			if (leftInput > 0 && rightInput == 0)
			{
				force += ((transform.right * factor * thrustFactor * leftInput) + (-1 * transform.up));
			}
			if (rightInput > 0 && leftInput == 0)
			{
				force += ((-1 * transform.right * factor * thrustFactor * rightInput) + (-1 * transform.up));
			}

			rb.AddForce(force, ForceMode.Force);
		}


		//ROTATION
		if (leftInput != 0 || rightInput != 0)
		{
			rb.AddTorque(0, 0, (-leftInput + rightInput) * rotationFactor, ForceMode.Force);
		}
		rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
	}

	public void StartGame()
	{
		StartCoroutine(StartSequence());
	}

	IEnumerator StartSequence()
	{
		canInput = false;
		thrustFactor = initialThrust;
		leftInput = 1;
		rightInput = 1;

		yield return new WaitForSeconds(startDuration);
		canInput = true;
		thrustFactor = origThrust;
		SwitchState(PlayerState.Flying);
		GameManager.instance.SetGameState(GameState.Game);

	}

	float CheckDistance()
	{
		Vector3 dir = transform.TransformDirection(Vector3.down);
		RaycastHit hit;
		if (Physics.Raycast(transform.position, dir, out hit, 50, mask))
		{
			SwitchState(PlayerState.Landing);
			return hit.distance;
		}
		SwitchState(PlayerState.Flying);
		return 100;
	}

	public override void Reset()
	{
		base.Reset();
		currentFuel = fuel;

	}

	public override void Disable()
	{
		rb.isKinematic = true;
		enabled = false;
		gameObject.SetActive(false);
		Reset();
	}


	void SwitchState(PlayerState state)
	{
		if (currentState != state)
		{
			lastState = currentState;
			
			currentState = state;

			switch (state)
			{
			case PlayerState.Intro:
				break;
			case PlayerState.Flying:
			
				break;
			case PlayerState.Landing:
			//put out landing gear
				break;
			case PlayerState.Landed:
				break;
			case PlayerState.Takeoff:
				break;
			case PlayerState.Crashed:
				canInput = false;
				break;
			case PlayerState.Dead:
				canInput = false;
				break;
			}
		}
	}

}
