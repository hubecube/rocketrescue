﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using FxProNS;

public class AdjustCameraBloom : MonoBehaviour
{
	FxPro camFx;

	public float startValue = 5;

	public BloomHelperParams bloomParams;
	public float thresh = 0;
	public float intensity = 3;
	public float softness = 3;

	public AnimationCurve animCurve;

	float origThreshold;
	float origIntensity;
	float origSoftness;

	void Awake()
	{
		camFx = GetComponent<FxPro>();
		origThreshold = camFx.BloomParams.BloomThreshold;
		origIntensity = camFx.BloomParams.BloomIntensity;
		origSoftness = camFx.BloomParams.BloomSoftness;
		bloomParams.BloomTint = camFx.BloomParams.BloomTint;
		bloomParams.BloomIntensity = intensity;
		bloomParams.BloomThreshold = thresh;
		bloomParams.BloomSoftness = softness;
		bloomParams.Quality = camFx.Quality;
	}

	void OnEnable()
	{
		
	}

	void OnDisable()
	{
		
	}

	void Update()
	{
		if (GameManager.instance.durationBeforeFlare <= startValue && GameManager.instance)
		{
			float t = Mathf.InverseLerp(5, 0, GameManager.instance.durationBeforeFlare);
			camFx.BloomParams.BloomThreshold = Mathf.Lerp(origThreshold, thresh, animCurve.Evaluate(t));
			camFx.BloomParams.BloomIntensity = Mathf.Lerp(origIntensity, intensity, animCurve.Evaluate(t));
			camFx.BloomParams.BloomSoftness = Mathf.Lerp(origSoftness, softness, animCurve.Evaluate(t));
			camFx.Init();
		}
		else
		{
			camFx.BloomParams.BloomThreshold = origThreshold;
			camFx.BloomParams.BloomIntensity = origIntensity;
			camFx.BloomParams.BloomSoftness = origSoftness;
		}
	}

}
