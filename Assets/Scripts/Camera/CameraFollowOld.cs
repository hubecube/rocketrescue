﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CameraFollowOld : MonoBehaviour
{

	public Transform target;
	public float lerpSpeed = 3;
	public float positionLerpSpeed = 10;
	public float maxFOV = 65;
	public float minFOV = 20;
	public float minSpeed = 0.1f;
	public float maxSpeed = 10;

	Quaternion currentRotation;
	Camera cam;
	Rigidbody rbTarget;

	float currentFOV;
	Vector3 targetPos;

	void Awake()
	{
		cam = GetComponent<Camera>();
		currentFOV = cam.fieldOfView;
		rbTarget = target.GetComponent<Rigidbody>();
	}


	void Update()
	{
		transform.LookAt(target);
		float speedLerp = Mathf.InverseLerp(minSpeed, maxSpeed, rbTarget.velocity.sqrMagnitude);
		float fovLerp = Mathf.Lerp(minFOV, maxFOV, speedLerp);

		cam.fieldOfView = fovLerp;

		targetPos = target.position;
		targetPos.z = transform.position.z;
		//		transform.position = targetPos;
		transform.position = Vector3.Lerp(transform.position, targetPos, positionLerpSpeed * Time.deltaTime);		


//		if ()
	}

	void LateUpdate()
	{
		
	}


}
